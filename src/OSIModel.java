import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class OSIModel {
	static String filename = "message.txt";
	public static char[] message = new char[80];
	public static String smtp = "Aprettynicesmtpprotocol";
	public static String NDR = "AydfNbddsa"; // presetation layer protocol
	public static String RTCP = "HHUhuhkjjkhuiuUu"; // session layer protocol. real time transport control
	public static String TCP = "sdfsfDDGgrrrf";
	public static int sourcePort = 20;
	public static int destPort = 25;
	public static String sourceIP = "198.10.13.230";
	public static String destIP = "199.10.17.175";
	public static String IPv4 = "lkiJNoonnk";
	public static String IEEE80211 = "Yjnkjhsui";
	
	// this layer will just take in the users message. Assuming its some type of email client 
	// so use SMTP protocol
	public static void applicationLayer(char[] input, int size) throws IOException {
		char[] messageANDAppProtocol = new char [smtp.length() + input.length];
		
		for (int i = 0; i < smtp.length(); i++ ) { 
			messageANDAppProtocol[i] = smtp.charAt(i);
		}
		for (int i = 0; i < message.length; i++ ) { 
			messageANDAppProtocol[i + smtp.length()] = input[i];
		}
		
	System.out.println(Arrays.toString(messageANDAppProtocol));
	
	presentationLayer(messageANDAppProtocol, messageANDAppProtocol.length);
	
	}
	
	// will add header and convert to ascii
	private static void presentationLayer(char[] input, int size) throws UnsupportedEncodingException {
		char [] messageANDPressProtocol = new char[NDR.length() + input.length];
		for (int i = 0; i < NDR.length(); i++ ) { 
			messageANDPressProtocol[i] = NDR.charAt(i);
		}
		for (int i = 0; i < input.length; i++ ) { 
			messageANDPressProtocol[i + NDR.length()] = input[i];
		}
		
		int ascii[] = new int [messageANDPressProtocol.length];
		for (int i = 0; i<messageANDPressProtocol.length; i++) { 
		 ascii [i] = (int)messageANDPressProtocol[i];
		}
		

		System.out.println(Arrays.toString(messageANDPressProtocol));
		System.out.println(Arrays.toString(ascii));
		sessionLayer(ascii, ascii.length);
	}

	
	private static void sessionLayer(int[] input, int length) {
		int[] messageANDSesProtocol = new int[RTCP.length() + input.length]; //  message so far with session protocol added
		for(int i = 0; i<RTCP.length(); i++) { 
			messageANDSesProtocol[i] = (int)RTCP.charAt(i);
		}
		
		for(int i = 0; i<input.length; i++){
			messageANDSesProtocol[i + RTCP.length()] = input[i];
		}
		
		System.out.println(Arrays.toString(messageANDSesProtocol));
		
		transportLayer(messageANDSesProtocol,messageANDSesProtocol.length);
	
	}

// add soruce and dest port to data
	private static void transportLayer(int[] input, int length) {
		int[] messageANDTransProtocol  = new int [TCP.length() + input.length +2];
		messageANDTransProtocol[0] = sourcePort;
		messageANDTransProtocol[1] = destPort;
		
		for(int i = 0; i < TCP.length(); i++) {
			messageANDTransProtocol[2+i] = (int)TCP.charAt(i);
		}
		
		for(int i = 0; i <input.length; i++ ) {
			messageANDTransProtocol[i + TCP.length()] = input[i];
		}
		
		int destPortbitSize = 4;
		int sourcePortbitSize = 4;
		int size = destPortbitSize + sourcePortbitSize + messageANDTransProtocol.length;
		System.out.println(Arrays.toString(messageANDTransProtocol));
		
		networkLayer(messageANDTransProtocol,size);
		
	}


	// add in source and dest ip adresses
	private static void networkLayer(int[] input, int size) { 
		int [] messageANDNetProtocol = new int[IPv4.length() + input.length + destIP.length() + sourceIP.length()];
		
		for(int i = 0; i<destIP.length(); i++) {
			messageANDNetProtocol[i] = (int)destIP.charAt(i);
		}
		
		for(int i = 0; i<sourceIP.length(); i++) {
			messageANDNetProtocol[i+destIP.length()] = (int)sourceIP.charAt(i);
		}
		
		for(int i = 0; i <IPv4.length(); i++){
			messageANDNetProtocol[i + destIP.length() + sourceIP.length()] = (int)IPv4.charAt(i);
		}
		
		for(int i = 0; i < input.length; i++) {
			messageANDNetProtocol[i + IPv4.length() + destIP.length() + sourceIP.length()] = input[i];
		}
		System.out.println(Arrays.toString(messageANDNetProtocol));
		
		int ipSizeBits = 2048; 
		int s = ipSizeBits + messageANDNetProtocol.length;
		datalinkLayer(messageANDNetProtocol, s);
	}
	// convert to bits and do bit stuffing
	private static void datalinkLayer(int[] input, int s) {
		int[] messageANDDataProtocol = new int [IEEE80211.length() + input.length];
		for(int i = 0; i<IEEE80211.length(); i++) {
			messageANDDataProtocol[i] = (int)IEEE80211.charAt(i);
		}
		for(int i = 0; i<input.length; i++) {
			messageANDDataProtocol[i + IEEE80211.length()] = input[i];
		}
		// convert ascii to binary
		String [] dataToSend = new String[messageANDDataProtocol.length];
		for(int i = 0; i<messageANDDataProtocol.length; i++) {
			dataToSend[i] = Integer.toBinaryString(messageANDDataProtocol[i]);
		}
		System.out.println(Arrays.toString(dataToSend));
		
		String d = Arrays.toString(dataToSend);
		
		String data = d.replace(",", "").replace(" ", "");
		System.out.println(data);
		String bitStuffed = data.replace("11111", "111110");
		System.out.println(bitStuffed);
	}


	public static void main (String[] args) throws IOException { 
		FileReader fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		String str;
		while ((str = br.readLine())!=null) {
			message = str.toCharArray();
		}
	applicationLayer(message, message.length);
	}
	
	
	

}
